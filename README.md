Documentation Swagger :
https://app.swaggerhub.com/apis/Dettri/APIscine/3.1#/

Déploiment du projet :

1. Récupérer le code présent sur le repository sur la branche master : https://gitlab.com/agm.gugliotta/apiscineproject

2. Installer l'Ingress : kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/master/deploy/static/provider/cloud/deploy.yaml

3. Créer l'image docker : docker build -t mynode:3.4 .

4. Appliquer l'image dans l'orchestrateur kubernetes : kubectl apply -f kubernetes.yml

5. L'API est accessible depuis localhost
