const http = require('http');
const url = require('url');
const service = require('./Service');
const mongoose = require('mongoose');

let apiName;
let apiversion;

//Fonction qui initialise le nom et la version de l'API
//@param ApiName Nom de l'API
//@param Version Version de l'API
exports.setApiNameAndVersion = function (ApiName, Version) {
    apiName = ApiName;
    apiversion = Version;
};

//Cree le serveur
exports.server = http.createServer((req, res) => {

    //Décompose l'url obtenue
    const reqUrl = url.parse(req.url, true);
    const path = '/' + apiName + '/' + apiversion + '/';

    //Si l'url n'est pas dans le bon format -> mauvaise requete
    if(!reqUrl.pathname.match( '(.*)' + path + '(.*)')) {
        service.invalidRequest(req, res);
    } else {
        //recupere le nom de la collection demandée dans l'url
        let controllerPath = reqUrl.pathname.slice(path.length);
        let modelName;

        if(controllerPath.match('(.*)/(.*)')) {
            const temp = controllerPath.split('/');
            modelName = temp[0];
        } else {
            modelName = controllerPath;
        }

        //Configure le modele selon le modele demandée en url
        const model = mongoose.model(modelName);

        // GET Endpoint
        if (req.method === 'GET') {
            console.log('Request Type:' +
                req.method + ' Endpoint: ' +
                reqUrl.pathname);

            service.getRequest(req, res, model, controllerPath);

            // POST Endpoint
        } else if (req.method === 'POST') {
            console.log('Request Type:' +
                req.method + ' Endpoint: ' +
                reqUrl.pathname);

            service.postRequest(req, res, model);

            // UPDATE EndPoint
        } else if (req.method === 'PUT') {
            console.log('Request Type:' +
                req.method + 'Endpoint: ' +
                reqUrl.pathname);

            service.putRequest(req, res, model, modelName);

            // DELETE EndPoint
        } else if (req.method === 'DELETE') {
            console.log('Request Type:' +
                req.method + 'Endpoint: ' +
                reqUrl.pathname);

            service.deleteRequest(req, res, model, modelName);

            // INVALID REQUEST
        }
    }
});


