// regex numerique
const numMin = /^\[.+\,\]/g; // [5,] valeur min = 5
const numMax = /^\[\,.+\]/g; // [,10] valeur max = 10
const numMinMax = /^\[.+\,.+\]/g; // [5,10] valeur entre borne

// regex date
const dateMin = /^\[\d+\-\d+\-\d+\,\]/g; // Piscine créé avant le {date}
const dateMax = /^\[\,\d+\-\d+\-\d+\]/g; // Piscine créé après le {date}
const dateMinMax = /^\[\d+\-\d+\-\d+\,\d+\-\d+\-\d+\]/g; // Piscine créé entre le {date 1} et {date 2}


//Fonction qui trie les items
//@param items récupérés de la bdd
//@param parametres de tri
exports.sortItems = function (sortParams) {
    // Decompose les parametres
    const params = sortParams.split(',');

    let sortObject = [];

    //Pour chaque parametre de tri
    params.forEach(param => {
        //Decompose le champ du tri et le sens du tri
        const splitParam = param.split(':');
        //Selon le sens du tri
        if(splitParam.length === 1 || splitParam[1] === 'asc') {
            //Tri les items recus selon le champ voulu par ordre croissant
            sortObject.push([splitParam[0],1]);
        } else if (splitParam[1] === 'desc') {
            //Tri les items recus selon le champ voulu par ordre décroissant
            sortObject.push([splitParam[0],-1]);
        }
    });

    //Retourne les items triés
    return sortObject;
};

exports.selectFields = function(fieldsParam){
  const params = fieldsParam.split(',');
  let selectObject = "";
    params.forEach(val => {
        selectObject += ' ';
        selectObject += val.toString();
    });
    return selectObject;
};

//Fonction qui exécute la pagination
//@param items récupérés de la bdd
//@param parametres de pagination
exports.paginate = function (items, rangeParams) {
    //Décompose les parametre pour avoir la range
    const range = rangeParams.split('-');
    const start = range[0] - 1;
    const end = range[1];

    //Découpe les données selon la range
    items = items.slice(start, end);

    //Renvoi des items paginés
    return items;
};

//Fonction qui récupere les parametres de filtres en excluant ceux de tri et de pagination
//@param queryParam query contenant les parametres
exports.getTypeParams = function (queryParam) {
    //Si dans la query on a un parametre de type sort on l'exclu
    let index = queryParam.indexOf('sort');
    if(index !== -1) {queryParam.splice(index, 1)}
    //Si dans la query on a un parametre de type range on l'exclu
    index = queryParam.indexOf('range');
    if(index !== -1) {queryParam.splice(index, 1)}
    //Si dans la query on a un parametre de type fields on l'exclu
    index = queryParam.indexOf('fields');
    if(index !== -1) {queryParam.splice(index, 1)}

    //Renvoi la query param
    return queryParam;
};


exports.removeSearchParams = function (queryParam, query) {
    let returnParam = [];
    queryParam.forEach(param => {
        if(!query[param].match(/\*.*\*/g)) {
            returnParam.push(param);
        }
    });
    return returnParam;
};

//Fonction qui crée le filtre de recherche
//@param query contenant les parametres
//@param query complete
exports.getFindObject = function (queryParam, query) {
    //Création de l'objet de filtre
    let findObject = {};

    //Pour chaque clé de parametre
    queryParam.forEach(key => {
        //On récupere la valeur de la clé
        let paramValue = query[key];
        let typeValue;

        //Selon le type de parametre on attribue la variable typeValue prend une valeur
        if (paramValue.match(/\-/g)) {
            typeValue = 0; // 0 = date
        } else if (paramValue.match(/\d/g)) {
            typeValue = 1; // 1 = number
        } else if (paramValue.match(/[aA-zZ]/g)) {
            typeValue = 2; // 2 = char or string
        }

        let params;

        //Selon le type de valeurs obtenu grâce à une comparaison à des variables regex prédéfinis on rentre dans un cas où on crée le filtre de recherche correspondant
        if (paramValue.match(numMin) && typeValue === 1) { // [x,] valeur min = x
            params = paramValue.match(/\d+/g); // = ["x"]
            //on recupere toutes les valeurs supérieurs à x
            findObject[key] = {$gte: parseInt(params[0])};
        } else if (paramValue.match(numMax) && typeValue === 1) { // [,x] valeur max = x
            params = paramValue.match(/\d+/g); // = ["x"]
            //on recupere toutes les valeurs inférieurs à x
            findObject[key] = {$lte: parseInt(params[0])};
        } else if (paramValue.match(numMinMax) && typeValue === 1) { // [x,u] valeurs entre bornes
            params = paramValue.match(/\d+/g); // = ["x","y"];
            //on recupere toutes les valeurs comprise entre x et y
            findObject[key] = {$gte: parseInt(params[0]), $lte: parseInt(params[1])};
        } else if (paramValue.match(dateMin) && typeValue === 0) { // [AAAA-MM-JJ,] crée après le AAAA-MM-JJ
            params = paramValue.match(/\d+\-\d+\-\d+/g); // = ["AAAA-MM-JJ"];

            //date de début depuis 0:0:0 pour englober tous les résultats
            const startDate = new Date(new Date(params).setHours(0, 0, 0));
            findObject[key] = {$gte: startDate};
        } else if (paramValue.match(dateMax) && typeValue === 0) { // [,AAAA-MM-JJ] créé avant le AAAA-MM-JJ
            params = paramValue.match(/\d+\-\d+\-\d+/g); // = ["AAAA-MM-JJ"];

            //date de fin jusqu'à 23:59:59 pour englober tous les résultats
            const endDate = new Date(new Date(params).setHours(23, 59, 59));

            findObject[key] = {$lte: endDate};
        } else if (paramValue.match(dateMinMax) && typeValue === 0) { // [AAAA-MM-JJ,AAAA-MM-JJ] créé entre le AAAA-MM-JJ et le AAAA-MM-JJ
            params = paramValue.match(/\d+\-\d+\-\d+/g); // = ["AAAA-MM-JJ,AAAA-MM-JJ"];

            //date de début depuis 0:0:0 et date de fin jusqu'à 23:59:59
            const startDate = new Date(new Date(params[0]).setHours(0, 0, 0));
            const endDate = new Date(new Date(params[1]).setHours(23, 59, 59));

            findObject[key] = {$gte: startDate, $lt: endDate};
            // Sinon si plusieurs valeurs recherchées
        } else if (paramValue.includes(",")) {
            //Décompose chaque valeurs
            const params = paramValue.split(',');

            //Selon le type de la valeur recherchée
            switch (typeValue) {
                case 0: // Si c'est plusieurs date
                    let dateParams = [];
                    params.forEach(param => {
                        let date = param.match(/\d+\-\d+\-\d+/g);

                        const startDate = new Date(new Date(date).setHours(0, 0, 0));
                        const endDate = new Date(new Date(date).setHours(23, 59, 59));
                        dateParams.push({[key]: {$gte: startDate, $lt: endDate}})
                    });
                    findObject = {$or: dateParams};
                    break;
                case 1: // Si c'est plusieurs valeurs numériques
                case 2: // Si c'est plusieurs chaines de caractères
                    findObject[key] = {$in: params};
                    break;
            }
            // Sinon une seule valeur recherchée n'ayant pas de regex précis
        } else {
            switch (typeValue) {
                case 0: // Si c'est une date
                    params = paramValue.match(/\d+\-\d+\-\d+/g);

                    //date de début depuis 0:0:0 et date de fin jusqu'à 23:59:59
                    const startDate = new Date(new Date(params).setHours(0, 0, 0));
                    const endDate = new Date(new Date(params).setHours(23, 59, 59));

                    findObject[key] = { $gte:startDate, $lte:endDate};
                    break;
                case 1: // Si c'est une valeur numérique
                case 2: // Si c'est une chaine de caractères
                    findObject[key] = query[key];
                    break;
            }
        }
    });

    //retourne le filtre de recherche
    return findObject;
};

//Fonction qui crée un objet de recherche
//@param query contenant les parametres
//@param query complete
exports.getSearchObject = function (queryParam, query) {
    //Création de l'objet de filtre
    var findObject = {};

    //Pour chaque clé de parametre
    queryParam.forEach(key => {
        //On récupere la valeur de la clé
        let paramValue = query[key];
        let paramsValue;

        //On décompose le parametre de la recherche
        if (paramValue.match(/\*.*\*/g)) {
            paramsValue = paramValue.split('*');
        } else {
            return;
        }

        //Si recherche totale
        if(paramsValue.length === 3) {
            findObject[key] = {$regex: paramsValue[1], $options: "i"};
            //Si recherche partiel
        } else if (paramsValue.length === 2) {
            //Si recherche au début
            if(paramsValue[0] === '') {
                findObject[key] = {$regex: '^' + paramsValue[1], $options: "i"};
                //Sinon recherche par la fin
            } else {
                findObject[key] = {$regex: paramsValue[0] +'$' , $options: "i"};
            }
        }
    });

    return findObject;
};
