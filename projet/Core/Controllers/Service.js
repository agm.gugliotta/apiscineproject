const url = require('url');
const helpers = require('./Helpers');
const mongoose = require('mongoose');

exports.getRequest = async function (req, res, itemModel, path) {
    const reqUrl = url.parse(req.url, true);
    const regex = /^\/.*\?/g;

    //si c'est une requete par ID on recupere l'ID
    let requestedId;
    const isNotSearch = path.match('search');
    const isRequest = path.indexOf('?');
    const isNotAll = path.indexOf('/');
    if((isNotSearch == null) && (isRequest === -1) && (isNotAll !== -1)) {
        const temp = path.split('/');
        requestedId = temp[1];
        //Si ID pas au bon format -> erreur 400
        try {
            requestedId = mongoose.Types.ObjectId(requestedId);
        } catch (e) {
            res.statusCode = 400;
        }
    }

    let response = {};
    let queryParam = helpers.getTypeParams(Object.keys(reqUrl.query));

    //Si on cherche par ID
    if(requestedId !== undefined) {
        await itemModel.findById(requestedId)
            .exec()
            .then(items => {
                response = items;
                //Si reponse différent de null on renvoi 200 sinon 404
                if(response !== null) {
                    res.statusCode = 200;
                } else {
                    res.statusCode = 404;
                }
            })
            .catch(err => {
                console.log(err);
                res.statusCode = 400;
            });
        //Sinon on cherche tous les objets
    } else {
        //Variable du filtre de la recherche
        let findObject = {};

        let searchObject = {};
        //Si on a un parametre de recherche on crée le filtre de recherche
        if(reqUrl.pathname.match("(.*)/search?(.*)")) {
            searchObject = helpers.getSearchObject(queryParam, reqUrl.query);
        }

        queryParam = helpers.removeSearchParams(queryParam, reqUrl.query);

        let filterObject = {};
        //Si on a des filtres dans l'url on va créer un le filtre de recherche
        if(queryParam.length > 0) {
            filterObject = helpers.getFindObject(queryParam, reqUrl.query);
        }

        let sortObject = [];
        //Si on a des parametres de tri dans l'url on va créer un objet de tri
        if(reqUrl.query.sort != null) {
            sortObject = helpers.sortItems(reqUrl.query.sort);
        }

        let selectObject = "";
        if(reqUrl.query.fields != null) {
            selectObject = helpers.selectFields(reqUrl.query.fields);
        }

        //On merge les 2 filtres si ils existent
        findObject = Object.assign(searchObject , filterObject);

        //Recupere les objets selon le filtre et si on a un parametre de tri ou de pagination on l'applique sur les données à renvoyer
        await itemModel.find(findObject)
            .collation({ locale: "fr" })
            .sort(sortObject)
            .select(selectObject)
            .exec()
            .then(items => {
                response = items;
                if((response !== null) && (response.length !== 0)) {
                    res.statusCode = 200;
                } else {
                    res.statusCode = 404;
                }

                if(reqUrl.query.range != null) {
                    response = helpers.paginate(response, reqUrl.query.range);
                    res.statusCode = 206;
                }
            })
            .catch(err => {
                console.log(err);
                res.statusCode = 400;
            });
    }

    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(response));
};

exports.postRequest = async function (req, res, itemModel) {
    var body = '';

    req.on('data', function (chunk) {
        body += chunk;
    });

    req.on('end', function () {

        var postBody = JSON.parse(body);
        postBody.CreatedAt = new Date();

        const Item = new itemModel(postBody);
        Item.save().then(result => console.log((result)));

        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        res.end(JSON.stringify(itemModel));
    });
};

exports.deleteRequest = async function (req, res, itemModel, itemModelName) {
    const reqUrl = url.parse(req.url, true);

    let index = reqUrl.path.indexOf(itemModelName);
    let id = reqUrl.path.substring(index + itemModelName.length + 1);

    if(id != null) {
        itemModel.findByIdAndDelete(id)
            .then(() => {
                res.statusCode = 200;
            })
            .catch(err => {
                console.log(err);
                res.statusCode = 400;
            });
        res.setHeader('Content-Type', 'application/json');
        res.end();
    }
    else
    {
        res.statusCode = 400;
        res.end();
    }
};

exports.putRequest = async function (req, res, itemModel, itemModelName) {
    const reqUrl = url.parse(req.url, true);
    var body = '';

    req.on('data', function (chunk) {
        body += chunk;
    });

    req.on('end', function () {
        var postBody = JSON.parse(body);
        let index = reqUrl.path.indexOf(itemModelName);
        let id = reqUrl.path.substring(index + itemModelName.length + 1);
        postBody.UpdatedAt = new Date();

        if (id != null) {
            itemModel.findByIdAndUpdate(mongoose.Types.ObjectId(id), postBody)
                .then(() => {
                    res.statusCode = 200;
                })
                .catch(err => {
                    console.log(err);
                    res.statusCode = 400;
                });

            res.setHeader('Content-Type', 'application/json');
            res.end();
        } else {
            res.statusCode = 400;
            res.end();
        }
    });
};

exports.invalidRequest = async function (req, res) {
    res.statusCode = 404;
    res.setHeader('Content-Type', 'text/plain');
    res.end('Invalid Request');
};
