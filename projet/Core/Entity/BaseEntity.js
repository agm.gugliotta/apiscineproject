const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let BaseEntity = new Schema({
    CreatedAt: Date,
    UpdatedAt: Date,
    DeletedAt: Date
});

module.exports = BaseEntity;
