const hostname = '';
const port = 3000;
const mongoose = require ('mongoose');

//const BaseController = require('apiscine_package');
const BaseController = require('./Core/Controllers/BaseController')

//Appel des modeles
require('./Services/Piscine/Models/Piscine');
require('./Services/Piscine/Models/Jacuzzi');

BaseController.setApiNameAndVersion('api', 'v1');

BaseController.server.listen(port, hostname, () => {
    //Connexion à la base mongodb
    mongoose.connect('mongodb+srv://admin:admin@cluster0-3qlul.gcp.mongodb.net/piscineDb?retryWrites=true&w=majority', {useFindAndModify: true, useUnifiedTopology:true, useNewUrlParser: true});
    console.log(`Server running at http://${hostname}:${port}/`);
});
