const mongoose = require('mongoose');
const extendSchema = require('mongoose-extend-schema');
const BaseEntity = require('../../../Core/Entity/BaseEntity')

let Jacuzzi = extendSchema(BaseEntity, {
    nom: {type: String, required: true},
    prix: {type: Number, required: true},
    hauteur: {type: Number, required: true},
    longueur: {type: Number, required: true},
    largeur: {type: Number, required: true},
});

module.exports = mongoose.model('jacuzzi', Jacuzzi);
